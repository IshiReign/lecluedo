package jeu.Personnage;

import java.io.Serializable;
import java.util.Objects;

public class Personnage implements Serializable {
	
	private static final long serialVersionUID = 1L;
	private String nom;
	private String dialogue;
	private Enigme enigme;
	private boolean enigmeresol = false;
	
	public Personnage(String nom, String dialogue, Enigme enigme) {
		this.nom = nom;
		this.dialogue = dialogue;
		this.enigme = enigme;
	}
	
	public int hashCode() {
		return Objects.hash(nom, dialogue);
	}
	
	/**Getter du nom du personnage
	 * @return Renvoie le nom du personnage
	 */
	public String getNom() {
		return this.nom;
	}
	
	
	/**Getter du diaogue du personnage
	 * @return Renvoie le dialogue du personnage
	 */
	public String getDialogue() {
		return this.dialogue;
	}
	
	/**Getter de l'�nigme
	 * @return Renvoie l'�nigme du personnage
	 */
	public Enigme getEnigme() {
		return this.enigme;
	}
	
	/**Setter du boolean de l'�nigme r�solue
	 * 
	 */
	public void enigmeResolue() {
		this.enigmeresol = true;
	}
	
	/**Getter de l'�nigme r�solue
	 * @return Renvoie la valeur du boolean �nigme r�solue
	 */
	public boolean getEnigmeResolue() {
		return this.enigmeresol;
	}
	
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		}
		if (!(o instanceof Personnage)) {
			return false;
		}
		Personnage p = (Personnage) o;
		return this.nom == p.getNom() && this.dialogue == p.getDialogue() && this.enigme == p.getEnigme();	
	}
}
