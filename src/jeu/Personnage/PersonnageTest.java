package jeu.Personnage;

import static org.junit.jupiter.api.Assertions.*;

import java.util.ArrayList;
import java.util.Arrays;


import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class PersonnageTest {
	Personnage p;
	@BeforeEach
	void setUp() throws Exception {
		p = new Personnage("perso", "blabla", new Enigme("question", new ArrayList<ReponsePossible>(Arrays.asList(new ReponsePossible("rep1", true), new ReponsePossible("rep2")))));
	}

	@Test
	void testHashCode() {
		Personnage p2 = new Personnage("perso", "blabla", new Enigme("question", new ArrayList<ReponsePossible>(Arrays.asList(new ReponsePossible("rep1", true), new ReponsePossible("rep2")))));
		assertEquals(p.hashCode(), p2.hashCode());
	}
	
	@Test
	void testGetNom() {
		assertEquals(p.getNom(), "perso");
	}

	@Test
	void testGetDialogue() {
		assertEquals(p.getDialogue(), "blabla");
	}

	@Test
	void testGetEnigme() {
		assertEquals(p.getEnigme(), new Enigme("question", new ArrayList<ReponsePossible>(Arrays.asList(new ReponsePossible("rep1", true), new ReponsePossible("rep2")))));
	}

	@Test
	void testEnigmeResolueAndgetResolue() {
		assertFalse(p.getEnigmeResolue());
		p.enigmeResolue();
		assertTrue(p.getEnigmeResolue());
		
	}
}
