package jeu.Personnage;


import java.io.Serializable;
import java.util.HashMap;
import java.util.Set;

import jeu.Zone;

public class ZonePersonnage extends Zone implements Serializable {
	
	private static final long serialVersionUID = 1L;
	private HashMap<Personnage, String> listpers;

	public ZonePersonnage(int id, HashMap<Personnage, String> listpers, String description, String image) {
		super(id, description, image);
		this.listpers = listpers;
		this.listpers = listpers;
	}
	
	/**Parcours la liste des personnages et renvoie le personnage en param�tre
	 * @param commande
	 * @return Le personnage en param�tre
	 */
	public Personnage parcoursListePers(String commande) {
		for (Personnage p : this.listpers.keySet()) {
			if (p.getNom().toUpperCase().equals(commande.toUpperCase())) {
				return p;
			}
			
		}
		return null;
	}

	/**Renvoie l'image du personnage plac� en param�tre
	 * @param personnageCourant
	 * @return L'image du personnage en param�tre
	 */
	public String deplacement(Personnage personnageCourant) {
		return this.listpers.get(personnageCourant);
		
	}

	public Set<Personnage> getPersonnage() {
		return listpers.keySet();	}
	

}
