package jeu.Personnage;

import static org.junit.jupiter.api.Assertions.*;

import java.util.ArrayList;
import java.util.Arrays;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class EnigmeTest {
	Enigme e;

	@BeforeEach
	void setUp() throws Exception {
		e = new Enigme("question", new ArrayList<ReponsePossible>(Arrays.asList(new ReponsePossible("rep1", true), new ReponsePossible("rep2"))));
	}

	@Test
	void testVerifReponse() {
		assertFalse(e.verifReponse(2));
		assertTrue(e.verifReponse(1));
	}

	@Test
	void testGetQuestion() {
		assertEquals(e.getQuestion(), "question");
	}

	@Test
	void testGetReponses() {
		assertEquals(e.getReponses(), "1/ rep1\n"
				+ "2/ rep2\n\n" );
	}

	@Test
	void testEqualsObject() {
		assertEquals(e, new Enigme("question", new ArrayList<ReponsePossible>(Arrays.asList(new ReponsePossible("rep1", true), new ReponsePossible("rep2")))));
	}

}
