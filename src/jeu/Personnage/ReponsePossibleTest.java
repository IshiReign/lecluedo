package jeu.Personnage;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class ReponsePossibleTest {
	ReponsePossible r;

	@BeforeEach
	void setUp() throws Exception {
		r = new ReponsePossible("rep", true);
	}


	@Test
	void testReponseJuste() {
		assertTrue(r.reponseJuste());
	}

	@Test
	void testGetReponse() {
		assertEquals(r.getReponse(), "rep");
	}

	@Test
	void testEqualsObject() {
		assertEquals(r, new ReponsePossible("rep", true));
	}

}
