package jeu.Personnage;

import static org.junit.jupiter.api.Assertions.*;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class ZonePersonnageTest {
	ZonePersonnage zp;
	

	@BeforeEach
	void setUp() throws Exception {
		HashMap<Personnage, String> h = new HashMap<>();
		h.put(new Personnage("perso", "blabla", new Enigme("question", new ArrayList<ReponsePossible>(Arrays.asList(new ReponsePossible("rep1", true), new ReponsePossible("rep2"))))), "string");
		zp = new ZonePersonnage(1, h, "description", "image");
	}

	
	@Test
	void testParcoursListePers() {
		assertEquals(zp.parcoursListePers("perso"), new Personnage("perso", "blabla", new Enigme("question", new ArrayList<ReponsePossible>(Arrays.asList(new ReponsePossible("rep1", true), new ReponsePossible("rep2"))))));
		assertEquals(zp.parcoursListePers("perso2"), null);
	}

	@Test
	void testDeplacement() {
		assertEquals(zp.deplacement(new Personnage("perso", "blabla", new Enigme("question", new ArrayList<ReponsePossible>(Arrays.asList(new ReponsePossible("rep1", true), new ReponsePossible("rep2")))))), "string");
	}

	@Test
	void testGetPersonnage() {
		HashSet<Personnage> h = new HashSet<>();
		h.add(new Personnage("perso", "blabla", new Enigme("question", new ArrayList<ReponsePossible>(Arrays.asList(new ReponsePossible("rep1", true), new ReponsePossible("rep2"))))));
		assertEquals(zp.getPersonnage(), h); 
	}

}
