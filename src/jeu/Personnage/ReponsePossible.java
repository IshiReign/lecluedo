package jeu.Personnage;

import java.io.Serializable;

public class ReponsePossible implements Serializable {
	
	private static final long serialVersionUID = 1L;
	private String reponse;
	private boolean bonneReponse;
	
	public ReponsePossible(String reponse) {
		this.reponse = reponse;
		this.bonneReponse = false;
	}
	
	public ReponsePossible(String reponse, boolean juste) {
		this.reponse = reponse;
		this.bonneReponse = juste;
	}
	
	/**Getter du boolean bonneReponse
	 * @return Renvoie la veleur du boolean bonneReponse
	 */
	public boolean reponseJuste() {
		return bonneReponse;
	}
	
	/**Getter de la r�ponse
	 * @return Renvoie la r�ponse
	 */
	public String getReponse() {
		return this.reponse;
	}
	
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		}
		if (!(o instanceof ReponsePossible)) {
			return false;
		}
		ReponsePossible rp = (ReponsePossible) o;
		return rp.getReponse() == this.reponse && rp.reponseJuste() == this.bonneReponse;
	}

}
