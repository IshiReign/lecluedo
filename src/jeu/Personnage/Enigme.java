package jeu.Personnage;

import java.io.Serializable;
import java.util.ArrayList;

public class Enigme implements Serializable {
	
	private static final long serialVersionUID = 1L;
	private String question;
	private ArrayList<ReponsePossible> listereponse;
	
	public Enigme(String question, ArrayList<ReponsePossible> listereponse) {
		this.question = question;
		this.listereponse = listereponse;
	}
	

	/**V�rifie la r�ponse entr� par l'utilisateur
	 * @param rep la r�ponse de l'utilisateur
	 * @return Si la r�ponse est correct
	 */
	public boolean verifReponse(int rep) {
		int reponse = rep - 1;
		return this.listereponse.get(reponse).reponseJuste();
		
	}
	
	/** Getter de la question
	 * @return Renvoie la question
	 */
	public String getQuestion() {
		return this.question;
	}
	
	/** Getter des r�ponses � une question
	 * @return Renvoie les diff�rentes r�ponses
	 */
	public String getReponses() {
		String res = "";
		System.out.println(listereponse.size());
		for (int i = 0; i < listereponse.size(); i++) {
			res += (i + 1) + "/ " + listereponse.get(i).getReponse() + "\n";
		}
		return res + "\n";
	}
	
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		}
		if (!(o instanceof Enigme)) {
			return false;
		}
		Enigme e = (Enigme) o;
		return e.getQuestion() == this.question && e.listereponse.equals(this.listereponse);
	}
 
	
	

}
