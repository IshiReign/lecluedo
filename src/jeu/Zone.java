package jeu;
import java.io.Serializable;
import java.util.HashMap;


public class Zone implements Serializable
{
		

	private static final long serialVersionUID = 1L;
	private int id;
    	private String description;
    	private String nomImage;
    	private HashMap<String, Zone> sorties;   

    	public Zone(int id, String description, String image) {
    		this.id = id;
    		this.description = description;
    		nomImage = image;
    		sorties = new HashMap<>();
    	}

    /**Ajoute une sortie plac� en param�tre � une seconde sortie plac� en param�tre, mais fait aussi l'inverse.
     * @param sortie
     * @param zoneVoisine
     */
    	public void ajouteSortie(Sortie sortie, Zone zoneVoisine) {
    		sorties.put(sortie.name(), zoneVoisine);
    		zoneVoisine.sorties.put(Sortie.inverse(sortie).name(), this);
    	}
   
    /**Setter d'une image
     * @param nomImage
     */
    	public void setNomImage(String nomImage) {
    		this.nomImage = nomImage;
    	}

    /**Getter de l'image
     * @return Renvoie l'image
     */
    	public String nomImage() {
        	return nomImage;
    	}
     
    /**Renvoie la description d'une zone
     *
     */
    	public String toString() {
        	return description;
    	}

    /**Renvoie une description rallong� de l'image
     * @return Description de la zone et les sorties
     */
    	public String descriptionLongue()  {
        	return "Vous etes dans " + description + "\nSorties : " + sorties();
    	}

    /**Getter des sorties
     * @return Renvoie les sorties possibles
     */
    	private String sorties() {
        	return sorties.keySet().toString();
    	}

    /**Renvoie les sorties de l'objet plac� en param�tre
     * @param direction
     * @return Sorties vis � vis du param�tre
     */
    	public Zone obtientSortie(String direction) {
    		return sorties.get(direction);
    	}
    
    /**Getter de l'ID d'une zone
     * @return renvoie l'ID
     */
    	public int getId() {
    		return this.id;
    	}
}

