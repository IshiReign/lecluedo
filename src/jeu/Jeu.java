package jeu;

import java.io.Externalizable;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectInputStream;
import java.io.ObjectOutput;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Timer;
import java.util.TimerTask;

import jeu.InventaireItem.*;
import jeu.Personnage.*;



public class Jeu implements Externalizable {
	
	private Personnage personnageCourant;
	private ObjetATrouve objetJoueur;
	private Inventaire inventaireJoueur;
	private ArrayList<Zone> listeZonesPrecedentes;
    	private GUI gui; 
    	private Zone zonePrecedente;
	private Zone zoneCourante;
	private boolean checknomparler = false;
    	private boolean checkreponse = false;
    	private int erreurJoueur;
    	private boolean fin = false;
    	private int tempsRestant = 300;
    	public static int erreurMAX = 3;
    	public Jeu() {
    		creerCarte();
    		gui = null;
    	}

    	public void setGUI( GUI g) { gui = g; afficherMessageDeBienvenue(); }
    
    	/**Classe qui cr�e les zones, les personnages et instancie l'inventaire du joueur
    	 * 
    	 */
    	private void creerCarte() {
    		erreurJoueur = 0;
    		objetJoueur = new ObjetATrouve();
    		Zone [] zones = new Zone [10];
    	
    	
    		HashMap<Personnage, String>h0 = new HashMap<>();
    		HashMap<Personnage, String>h3 = new HashMap<>();
    		HashMap<Personnage, String>h4 = new HashMap<>();
    		HashMap<Personnage, String>h5 = new HashMap<>();
    		HashMap<Personnage, String>h6 = new HashMap<>();
    	
    		Personnage p1 = new Personnage("Colonel Moutarde", "Bonjour jeune homme\n", new Enigme("Qu'est-ce qui doit �tre cass� afin que tu puisses l'utiliser ?\n",
    			new ArrayList<ReponsePossible>(Arrays.asList(new ReponsePossible("Ta main\n"),
    			new ReponsePossible("Un oeuf\n", true), new ReponsePossible("Une branche\n")))));
    	
    		Personnage p2 = new Personnage("Madame Leblanc", "blablabla\n", new Enigme("Quel mois de l'ann�e compte 28 jours ?\n", 
    			new ArrayList<ReponsePossible>(Arrays.asList(new ReponsePossible("Septembre\n"),
    					new ReponsePossible("Juin\n"), new ReponsePossible("Fevrier", true)))));    	
    	
    		Personnage p3 = new Personnage("Monsieur Olive", "blablabla\n", new Enigme("Que peux-tu attraper mais jamais lancer ?\n", 
    			new ArrayList<ReponsePossible>(Arrays.asList(new ReponsePossible("Frisbee\n"),
    					new ReponsePossible("Exception\n"), new ReponsePossible("Grippe", true)))));
    	
    		Personnage p4 = new Personnage("Madame Rose", "blablabla\n", new Enigme("Il t'appartient, mais ce sont tes amis qui l'utilisent le plus souvent. De quoi s'agit-il ?\n", 
    			new ArrayList<ReponsePossible>(Arrays.asList(new ReponsePossible("Prenom\n", true),
    					new ReponsePossible("Argent\n"), new ReponsePossible("L'ordinateur")))));
    	
    		Personnage p5 = new Personnage("Monsieur Violet", "blablabla\n", new Enigme("Il n'y a qu'un seul mot dans le dictionnaire qui est orthographie mal. Quel est ce mot ?\n", 
    			new ArrayList<ReponsePossible>(Arrays.asList(new ReponsePossible("Erreur\n"),
    					new ReponsePossible("Mal\n", true), new ReponsePossible("Bien")))));
    	
    		Personnage p6 = new Personnage("Docteur Orchidee", "Bravo vous avez r�unis toutes les armes pr�sente dans la maison. \n" +
    					" Bien jou�, c'est moi la coupable, je vais vous donnez un indice sur l'arme du crime. \n", objetJoueur.enigmeAleatoire());
    			;
    		h3.put(p1, "bureau_detective_haut3.png");
    		h5.put(p2, "studio_detective_milieu.jpg");
    		h6.put(p3, "biblio_detective_haut2.png");
    		h0.put(p4, "grand_salon_detective_table.jpg");
    		h4.put(p5, "cuisine_detective_haut3.png");
    		h0.put(p6, "grand_salon_detective_canape.jpg");
    	
    	
    	
    	// 0) Grand Salon
        //zones[0] = new Zone("grand salon", "grand_salon_detective_haut.jpg" );
    		zones[0] = new ZonePersonnage(1, h0, "grand salon", "grand_salon_detective_haut.jpg" );
        //NORD-EST-SUD-OUEST
        //zone[0] = new ZonePersonnage((new Personnage("")))

        // 1) Salon
        //zones[1] = new Zone("salon", "salon_coffre_fermee_detective_droite.jpg" );
        	zones[1] = new ZoneCoffre(2, "salon", "salon_coffre_fermee_detective_droite.jpg", new Coffre(), "salon_coffre_fermee_detective_haut.jpg", "salon_coffre_ouvert_detective_haut.jpg", "salon_coffre_ouvert_detective_gauche.jpg");
        //EST
        // 2) Salle � manger
        //zones[2] = new Zone("salle � manger", "salle_a_manger_detective_haut.jpg" );
        	zones[2] = new Zone(3, "salle � manger", "salle_a_manger_detective_haut.jpg" );
        //OUEST-NORD
        // 3) Bureau
        //zones[3] = new Zone("bureau", "bureau_detective_bas.jpg");
        	zones[3] = new ZonePersonnage(4, h3, "bureau", "bureau_detective_bas3.png");
        //SUD
        //Cuisine
                zones[4] = new ZonePersonnage(5, h4, "cuisine", "cuisine_detective_bas3.png");
        
        // 5) Studio
        //zones[5] = new Zone("studio", "studio_detective_gauche.jpg");
                zones[5] = new ZonePersonnage(6, h5, "studio", "studio_detective_gauche.jpg");

        // 6) Biblioth�que
        //zones[6] = new Zone("biblioth�que", "biblio_detective_porte.jpg");
                zones[6] = new ZonePersonnage(7, h6, "biblioth�que", "biblio_detective_porte2.png");


        //Sorties Grand Salon
                zones[0].ajouteSortie(Sortie.OUEST, zones[1]); //Ouest vers Salon
                zones[0].ajouteSortie(Sortie.EST, zones[2]); //Est vers Salle a manger
                zones[0].ajouteSortie(Sortie.NORD, zones[3]); //Nord vers le Bureau
                zones[0].ajouteSortie(Sortie.SUD, zones[5]); //Sud vers Studio
                zones[2].ajouteSortie(Sortie.NORD, zones[4]); //Nord vers Cuisine
                zones[5].ajouteSortie(Sortie.EST, zones[6]); // Est vers Bibliotheque
                zoneCourante = zones[0];
                inventaireJoueur = new Inventaire();
                
                Timer time = new Timer();
                time.schedule(new TimerTask() {
                	public void run() {
                		tempsRestant--;
                		if (tempsRestant == 0 ) {
                			time.cancel();
                			gui.afficher("Le temps est �coul�");
                			terminer();
                		}
                	}
                }, 0, 1000);
                
                listeZonesPrecedentes = new ArrayList<>();
        
        
        
    	}
    
    
    	/**Affiche la zone courante et sa description (nom de zone et sorties)
    	 * 
    	 */
    	private void afficherLocalisation() {
            	gui.afficher( zoneCourante.descriptionLongue());
            	gui.afficher();
    	}

    	private void afficherMessageDeBienvenue() {
    		gui.afficher("Bienvenue !");
    		gui.afficher();
    		gui.afficher("Tapez '?' pour obtenir de l'aide.");
    		gui.afficher();
    		afficherLocalisation();
    		gui.afficheImage(zoneCourante.nomImage());
    	}
    
    	/**Ex�cution des commandes rentr�es par l'utilisateur selon celle plac�e en param�tre
    	 * @param commandeLue
    	 */
    	public void traiterCommande(String commandeLue) {
    		gui.afficher( "> " + commandeLue + "\n");
    		if (checknomparler) {
    			if (zoneCourante instanceof ZoneCoffre) {
    				interactionCoffre(commandeLue);
    			}
    			else {
    				verifPerso(commandeLue);
    			}
    		}
    		else if (checkreponse) {
    			try {
    				int r = Integer.parseInt(commandeLue);
    				if (personnageCourant.getEnigme().verifReponse(r)) {
    					personnageCourant.enigmeResolue();
    					try {
    						Item i = objetJoueur.objetTrouve();
    						inventaireJoueur.ajoute(i);
    						gui.afficher("Vous avez r�solu l'�nigme\n");
    						if (objetJoueur.size() != 0) {
    							gui.afficher("Bravo, je vous donne donc " + i.getArme() + ", c'est peut-�tre l'arme du crime");
						}
    						else if (fin) {
    							
							gui.afficher("Vous avez termin� le jeu, F�licitation !!!");
							terminer();
						}
	    				
					} catch (Exception e) {
						
						e.printStackTrace();
					}
    				
    					checkreponse = false;
    					personnageCourant = null;
    				}
    				else {
    					gui.afficher("La r�ponse est fausse\n");
    					erreurJoueur++;
    					if (erreurJoueur >= erreurMAX) {
    						gui.afficher("Vous avez perdu !");
    						terminer();
    					}
    					else {
    						gui.afficher("Il vous reste " + (erreurMAX - erreurJoueur) + " chance(s)");
    					}
    				}
    			
    			
    			}
    			catch (NumberFormatException e) {
    				gui.afficher("La r�ponse n'est pas un nombre\n");
    				gui.afficher("Resaisir une r�ponse");
    			}
    		}
    		else {
    			switch (commandeLue.toUpperCase()) {
    				case "?" : case "AIDE" : 
    					afficherAide(); 
    					break;
    				case "N" : case "NORD" :
    					allerEn( "NORD"); 
    					break;
    				case "S" : case "SUD" :
    					allerEn( "SUD"); 
    					break;
    				case "E" : case "EST" :
    					allerEn( "EST"); 
    					break;
    				case "O" : case "OUEST" :
    					allerEn( "OUEST"); 
    					break;
    				case "Q" : case "QUITTER" :
    					terminer();
    					break;
    				case "K" : case "INTERAGIR":
    					if (zoneCourante instanceof ZoneCoffre) {
    						checknomparler = true;
    						ZoneCoffre z = (ZoneCoffre) zoneCourante;
    						Coffre c = z.getCoffre();
    						gui.afficher(c.getEnigme().getQuestion());
    						gui.afficher(c.getEnigme().getReponses());
    						gui.afficheImage(z.getCoffreF());
            		
    					}
    					else {
    						gui.afficher("Il n'y a pas de coffre dans cette zone");
    					}	
            	
            	
    					break;
    				case "P" : case "PARLER":
    					if (zoneCourante instanceof ZonePersonnage) {
    						checknomparler = true;
    						for (Personnage p : ((ZonePersonnage) zoneCourante).getPersonnage()) {
    							gui.afficher(p.getNom() + "\n");
    						}
    						gui.afficher("Entrer le nom du Personnage");
            		
    					}
    					else {
    						gui.afficher("Cette zone ne contient pas de personnage");
    					}
    					break;
    				case "I" : case "INVENTAIRE" :
    					gui.afficher(inventaireJoueur.afficherInventaire());
    					break;
    				case "C" : case "COUPABLE" :
    					if (zoneCourante.getId() == 1 && objetJoueur.size() == 0) {
    						checknomparler = true;
    						fin = true;
    						verifPerso("Docteur Orchidee");
    					}
    					break;
    				case "T" : case "TEMPS" :
    					gui.afficher("Temps restant : " + tempsRestant + " secondes");
    					break;
    				case "V" : case "SAUVEGARDER" :
    					try {
    						this.save();
    					} catch (IOException e) {
    						e.printStackTrace();
    					}
    					break;
    				case "H" : case "CHARGER" :
    					try {
    						this.load();
    					} catch (ClassNotFoundException e) {
    						e.printStackTrace();
    					} catch (IOException e) {
    						e.printStackTrace();
    					}
    					break;
    				case "R" : case "RETOUR" :
    					if (!listeZonesPrecedentes.isEmpty()) {
    					        zoneCourante = listeZonesPrecedentes.get(listeZonesPrecedentes.size() - 1);
    					        this.setGUI(this.gui);
    					        this.listeZonesPrecedentes.remove(listeZonesPrecedentes.size() - 1);
    					        System.out.println(listeZonesPrecedentes);
    					        break;
    					}
    					else 
    					{
    						this.gui.afficher("IMPOSSIBLE D'ALLER PLUS EN ARRI�RE.");
    						gui.afficher(zoneCourante.descriptionLongue());
    						gui.afficher();
    						gui.afficheImage(zoneCourante.nomImage());
    						break;
    					}
    				default : 
    					gui.afficher("Commande inconnue");
    					break;
    			}
    		
    		}
        
    	}
    
    /**Classe qui r�pertorie tous les comportements possible vis � vis du coffre
     * @param reponse
     */
    	private void interactionCoffre(String reponse) {
    		ZoneCoffre z = (ZoneCoffre) zoneCourante;
    		if (z.getEnigmeResolue()) {
    			gui.afficher("L'�nigme a d�j� �t� r�solue\n");
    			checknomparler = false;
    		}
    		else {
    			try {
    				int r = Integer.parseInt(reponse);
    				if (z.getCoffre().getEnigme().verifReponse(r)) {
    					z.enigmeResolue();
    					try {
    						Item i = objetJoueur.objetTrouve();
						inventaireJoueur.ajoute(i);
						gui.afficher("Vous avez r�solu l'�nigme\n");
	    					gui.afficher("Bravo, je vous donne donc " + i.getArme() + ", c'est peut-�tre l'arme du crime");
	    					gui.afficheImage(z.getCoffreO());
    					} catch (Exception e) {
						
    						e.printStackTrace();
    					}
    					checknomparler = false;
    				}
    				else {
    					gui.afficher("La r�ponse est fausse\n");
    					erreurJoueur++;
    				}
    			
    			}
    			catch (NumberFormatException e) {
    				gui.afficher("La r�ponse n'est pas un nombre\n");
    				gui.afficher("Resaisir une r�ponse");
    			}
    		
    		}
		
		
	}

    	/**V�rifie avec quel perso on interagit
    	 * @param commandeLue
    	 */
    	private void verifPerso(String commandeLue) {
    		ZonePersonnage z = (ZonePersonnage) zoneCourante;
		/*if(commandeLue.toUpperCase() == "C" || commandeLue.toUpperCase() == "COUPABLE") {
			VerifPerso("Docteur Orchidee");
			return;
		}*/
    		personnageCourant = z.parcoursListePers(commandeLue);
    		if (personnageCourant == null) {
    			gui.afficher(commandeLue + " n'est pas dans la zone");
    		
    		}
    		else if (personnageCourant.getNom() == "Docteur Orchidee" && !fin) {
    			gui.afficher("Vous ne pouvez pas encore lui parler, veuillez r�soudre les autres �nigmes");
    		}
    		else if (checknomparler) {
    			checknomparler = false;
    			gui.afficheImage(z.deplacement(personnageCourant));
    			if (personnageCourant.getEnigmeResolue()) {
    				gui.afficher("L'enigme a d�j� �t� resolue\n");
    			}
    			else {
    				gui.afficher(personnageCourant.getDialogue());
    				gui.afficher(personnageCourant.getEnigme().getQuestion());
    				gui.afficher(personnageCourant.getEnigme().getReponses());
    				checkreponse = true;
    			}
    		
    		
    		}
    	
    		
    		
    	}
	
	/**M�thode qui r�cup�re les diff�rents objets de la partie en cours
	 *
	 */
	public void writeExternal(ObjectOutput out) throws IOException {
        	out.writeObject(this.gui);
        	out.writeObject(this.inventaireJoueur);
        	out.writeObject(this.erreurJoueur);
        	out.writeObject(this.objetJoueur);
        
    	}

    	/**M�thode qui lit les objets r�cup�rer dans la partie en cours
    	 *
    	 */
    	public void readExternal(ObjectInput in) throws IOException, ClassNotFoundException {
    		this.gui = (GUI) in.readObject();
    		this.inventaireJoueur = (Inventaire) in.readObject();
    		this.erreurJoueur = (Integer) in.readObject();
    		this.objetJoueur = (ObjetATrouve) in.readObject();
        
    	}
    
    	/**M�thode qui va sauvegarder la partie en cours
    	 * @throws IOException
    	 */
    	public void save() throws IOException {

    		FileOutputStream fileOutputStream = new FileOutputStream("jeuSave.txt"); //le fichier "jeuSave.txt" est le fichier o� se trouve la sauvegarde, il se trouve dans le repertoire Jeu
    		ObjectOutputStream objectOutputStreamGuiSave = new ObjectOutputStream(fileOutputStream);

    		this.writeExternal(objectOutputStreamGuiSave);

    		objectOutputStreamGuiSave.flush();
    		objectOutputStreamGuiSave.close();
    		fileOutputStream.close();

    		this.gui.afficher("Partie sauvegard�e");

    	}
    
 // La m�thode pour charger la sauvegarde

    	/**M�thode qui va charger la partie sauvegard�e
    	 * @throws IOException
    	 * @throws ClassNotFoundException
    	 */
    	public void load() throws IOException, ClassNotFoundException {

    		File file = new File("jeuSave.txt");
    		// on v�rifie que le fichier n'est pas vide. Sinon on ne chargera rien
    		if (file.length() == 0) 
    		{ 
    			this.gui.afficher("AUCUNE SAUVEGARDE TROUV�E! SAUVEGARDEZ AU MOINS UNE FOIS POUR CHARGER"); 
    		} 
    		else
    		{

    			Jeu j = new Jeu();
    			FileInputStream fileInputStream = new FileInputStream("jeuSave.txt");
    			ObjectInputStream objectInputStream = new ObjectInputStream(fileInputStream);
    			j.readExternal(objectInputStream);
    			objectInputStream.close();
    			fileInputStream.close();
    			j.setGUI(this.gui);
    			this.inventaireJoueur = j.getInventaireJoueur();
    			this.erreurJoueur = j.getErreurJoueur();
    			this.objetJoueur = j.objetJoueur;
    			zoneCourante = j.getZoneCourante(); //on change la zone courante sinon le jeu pense qu'on est dans la pi�ce avant la charge 
    			this.gui.afficher("Partie Charg�");
    		}

    	}

    

	/**Getter de la zone courante
	 * @return Renvoie la zone courante dans la partie
	 */
	private Zone getZoneCourante() {
		return this.zoneCourante;
	}

	/**Getter des erreurs du joueur
	 * @return Renvoie le nombre d'erreur du joueur
	 */
	private int getErreurJoueur() {
		return this.erreurJoueur;
	}

	/**Getter de l'inventaire du joueur
	 * @return Renvoie l'inventaire du joueur � un instant T
	 */
	private Inventaire getInventaireJoueur() {
		return this.inventaireJoueur;
	}

	/**Renvoie les commandes ex�cutables dans le jeu
	 * 
	 */
	private void afficherAide() {
        	gui.afficher("Etes-vous perdu ?");
        	gui.afficher();
        	gui.afficher("Les commandes autorisées sont :");
        	gui.afficher();
        	gui.afficher(Commande.toutesLesDescriptions().toString());
        	gui.afficher("Vous devez avoir parler avec tous les autres personnages avant d'interagir avec Docteur Orchidee");
        	gui.afficher();
    	}

    	/**Permet de changer de zone
    	 * @param direction
    	 */
    	private void allerEn(String direction) {
    		Zone nouvelle = zoneCourante.obtientSortie( direction);
    		if ( nouvelle == null ) {
    			gui.afficher( "Pas de sortie " + direction);
    			gui.afficher();
    		}
    		else {
    			zonePrecedente = zoneCourante; // je garde ainsi en m�moire la zone precedente
    			listeZonesPrecedentes.add(zonePrecedente); //je l'ajoute a liste des zones precedentes
    			zoneCourante = nouvelle;		
    			gui.afficher(zoneCourante.descriptionLongue());
    			gui.afficher();
    			gui.afficheImage(zoneCourante.nomImage());
    			
    		}
    	}
    
    	/**Met fin au jeu, on ne peut plus �crire dans la console
    	 * 
    	 */
    	private void terminer() {
    		gui.afficher( "Au revoir...");
    		gui.enable( false);
    	}
}
