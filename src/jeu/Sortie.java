package jeu;
public enum Sortie {
	NORD, SUD, EST, OUEST;
	
	
	/**Renvoie la sortie plac� en param�tre
	 * @param sortie
	 * @return Selon les cardinalit�s (Nord, Sud, Est, Ouest)
	 */
	public static Sortie inverse(Sortie sortie) {
		if (sortie == NORD) {
			return SUD;
		}
		else if (sortie == OUEST) {
			return EST;
		}
		else if (sortie == SUD) {
			return NORD;
			
		}
		return OUEST;
	}
}
