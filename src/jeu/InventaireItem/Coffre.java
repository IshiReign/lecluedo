package jeu.InventaireItem;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;


import jeu.Personnage.Enigme;
import jeu.Personnage.ReponsePossible;

public class Coffre implements Serializable {
	

	private static final long serialVersionUID = 1L;
	private Enigme enigme;
	
	
	public Coffre() {
		enigme = randomEnigme();
	}
	
	/**Renvoie une �nigme au hasard parmis celle instanci� dans la m�thode
	 * @return Renvoie une �nigme
	 */
	public static Enigme randomEnigme() {
		ArrayList<Enigme> listeEnigme = new ArrayList<>();
		listeEnigme.add(new Enigme("Je commence la nuit et je termine le matin. Qui suis-je ?\n", new ArrayList<ReponsePossible>(Arrays.asList(new ReponsePossible("La lune\n"), new ReponsePossible("Le N\n", true), new ReponsePossible("Le M\n")))));
		listeEnigme.add(new Enigme("Quel chiffre obtient-on en multipliant tous les chiffres d'un clavier ?\n", new ArrayList<ReponsePossible>(Arrays.asList(new ReponsePossible("90\n"), new ReponsePossible("950\n"), new ReponsePossible("0\n", true)))));
		listeEnigme.add(new Enigme("Je transforme une plante en une plan�te. Qui suis-je ?\n", new ArrayList<ReponsePossible>(Arrays.asList(new ReponsePossible("la lettre e\n", true), new ReponsePossible("Thanos\n"), new ReponsePossible("Le jardinnier\n", true)))));
		listeEnigme.add(new Enigme("Tu participes � une course et, � la toute fin, tu d�passes la personne qui occupe la deuxi�me place. � quelle place as-tu termin� la course ?\n", 
				new ArrayList<ReponsePossible>(Arrays.asList(new ReponsePossible("Premier\n"), new ReponsePossible("Deuxi�me\n", true), new ReponsePossible("Troisi�me\n")))));
		Collections.shuffle(listeEnigme);
		return listeEnigme.get(0);
		
	}
	
	/**Getter de l'�nigme
	 * @return Renvoie l'�nigme
	 */
	public Enigme getEnigme() {
		return this.enigme;
	}
	

}
