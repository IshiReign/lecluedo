package jeu.InventaireItem;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class ItemTest {
	Item i;

	@BeforeEach
	void setUp() throws Exception {
		i = new Item("arme", "phrase");
	}

	

	@Test
	void testGetArme() {
		assertEquals(i.getArme(), "arme");
	}

	@Test
	void testCompareTo() {
		Item i2 = new Item("arme", "phrase");
		assertTrue(i.compareTo(i2) == 0);
	}

	@Test
	void testEqualsObject() {
		assertEquals(i, new Item("arme", "phrase"));
	}

	@Test
	void testGetPhrase() {
		assertEquals(i.getPhrase(), "phrase");
	}

}
