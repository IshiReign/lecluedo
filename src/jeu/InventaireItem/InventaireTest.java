package jeu.InventaireItem;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class InventaireTest {
	Inventaire inv;

	@BeforeEach
	void setUp() throws Exception {
		inv = new Inventaire();
	}

	@Test
	void testAjouteAndAffiche() {
		inv.ajoute(new Item("objet", "phrase"));
		assertEquals(inv.afficherInventaire(), "objet ");
	}

}
