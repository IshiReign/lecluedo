package jeu.InventaireItem;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Iterator;

public class Inventaire implements Iterable<Item>, Serializable {

	private static final long serialVersionUID = 1L;
	private ArrayList<Item> listeItem;
	
	public Inventaire() {
		listeItem = new ArrayList<>();
	}

	@Override
	public Iterator<Item> iterator() {
		return listeItem.iterator();
	}
	
	
	/**Ajoute un Item plac� en param�tre dans la liste
	 * @param i
	 */
	public void ajoute(Item i) {
		
		listeItem.add(i);
	
		
	}

	/**Affiche l'inventaire
	 * @return renvoie la liste d'objet
	 */
	public String afficherInventaire() {
		String res = "";
		for (int i = 0; i < listeItem.size(); i++) {
			res += listeItem.get(i) + " "; 
		}
		return res;
	}
	
	
	
	
}
