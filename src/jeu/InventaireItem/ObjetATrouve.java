package jeu.InventaireItem;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Random;

import jeu.Personnage.Enigme;
import jeu.Personnage.ReponsePossible;

public class ObjetATrouve implements Iterable<Item>, Serializable {
	
	private static final long serialVersionUID = 1L;
	private LinkedList<Item> listeobjets;
	
	public ObjetATrouve() {
		
		listeobjets = new LinkedList<Item>();
		listeobjets.push(new Item("Chandelier", "Qu'est-ce qui ressemble � un trident mais fait de la lumi�re ?\n")); 
		listeobjets.push(new Item("Clef Anglaise", "Quelle clef poss�de une nationalit�e ?\n"));
		listeobjets.push(new Item("Corde", "Quel �l�ment retrouve-t-on sur une guitarre ?\n"));
		listeobjets.push(new Item("Maltraque", "Quelle arme utilise les policiers ?\n"));
		listeobjets.push(new Item("Poignard", "Qu'est-ce qui tranche et est utilis� par les assassins")); 
		listeobjets.push(new Item("Revolver", "Quelle arme utilise de la poudre?\n"));
	}

	@Override
	public Iterator<Item> iterator() {
		this.listeobjets.iterator();
		return null;
	}
	
	/**Renvoie le premier objet apr�s m�lange de la liste d'objet
	 * @return Renvoie l'extraction d'un objet de la liste
	 */
	public Item objetTrouve() {
		if (listeobjets.size() == 0) {
			return null;
		}
		Collections.shuffle(listeobjets);
		return this.listeobjets.poll();
		
	}

	/**Getter de la taille de la liste
	 * @return Renvoie la taille de la liste
	 */
	public int size() {
		return listeobjets.size();
	}
	
	/**Renvoie une �nigme au hasard parmis celle instanci� dans le constructeur
	 * @return Renvoie une �nigme associ�e � un Item
	 */
	public Enigme enigmeAleatoire() {
		Random rand = new Random();
		Item i = this.listeobjets.get(rand.nextInt(listeobjets.size()));
		String question = i.getPhrase();
		ArrayList<ReponsePossible> listerep = new ArrayList<>();
		for (Item it : listeobjets) {
			listerep.add(new ReponsePossible(it.getArme(), it.equals(i)));	
		}
		return new Enigme(question, listerep);
	}

}
