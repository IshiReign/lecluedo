package jeu.InventaireItem;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class ZoneCoffreTest {
	ZoneCoffre zc;

	@BeforeEach
	void setUp() throws Exception {
		zc = new ZoneCoffre(1, "description", "image1", new Coffre(), "image2", "image3", "image4");
	}


	@Test
	void testGetCoffreO() {
		assertEquals(zc.getCoffreO(), "image3");
	}

	@Test
	void testGetCoffreF() {
		assertEquals(zc.getCoffreF(), "image2");
	}

	@Test
	void testGetEnigmeResolueAndEnigmeResolue() {
		assertFalse(zc.getEnigmeResolue());
		zc.enigmeResolue();
		assertTrue(zc.getEnigmeResolue());
		
	}


}
