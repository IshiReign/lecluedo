package jeu.InventaireItem;

import java.io.Serializable;

public class Item implements Comparable<Item>, Serializable {
	
	private static final long serialVersionUID = 1L;
	private String arme;
	private String phrase;
	
	public Item(String arme, String phrase) {
		this.arme = arme;	
		this.phrase = phrase;
	}
	
	/**Getter de l'arme
	 * @return Renvoie l'arme
	 */
	public String getArme() {
		return this.arme;
	}

	@Override
	public int compareTo(Item o) {
		return this.arme.compareTo(o.arme);
	}
	
	@Override
	public String toString() {
		return this.getArme();
	}
	
	@Override
	
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		}
		if (!(o instanceof Item)) {
			return false;
		}
		Item it = (Item) o;
		return it.arme == this.arme && it.phrase == this.phrase;
	}
	
	/**Getter de la phrase
	 * @return Renvoie la phrase li�e � l'objet
	 */
	public String getPhrase() {
		return this.phrase;
	}
	
	

	
	
}
