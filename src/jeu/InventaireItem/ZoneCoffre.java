package jeu.InventaireItem;

import java.io.Serializable;

import jeu.Zone;

public class ZoneCoffre extends Zone implements Serializable {
	
	private static final long serialVersionUID = 1L;
	private Coffre coffreZ;
	private String imageCoffreO;
	private String imageCoffreF;
	private String imageCoffreOPorte;
	private boolean enigmeresol = false;
	
	public ZoneCoffre(int id, String description, String image, Coffre coffreZ, String imageCoffreF, String imageCoffreO, String imageCoffreOPorte) {
		super(id, description, image);
		this.imageCoffreF = imageCoffreF;
		this.imageCoffreO = imageCoffreO;
		this.imageCoffreOPorte = imageCoffreOPorte;
		this.coffreZ = coffreZ;
	}
	
	/**Renvoie l'objet coffre de la zone
	 * @return
	 */
	public Coffre getCoffre() {
		return this.coffreZ;
	}
	
	/**Renvoie une image avec le coffre ouvert
	 * @return
	 */
	public String getCoffreO() {
		return this.imageCoffreO;
		
	}
	
	/**Renvoie une image avec le coffre ferm�e
	 * @return
	 */
	public String getCoffreF() {
		return this.imageCoffreF;
	}
	
	/**Getter de l'�nigme r�solue
	 * @return Renvoie la valeur du boolean de l'�nigme r�solue
	 */
	public boolean getEnigmeResolue() {
		return this.enigmeresol;
	}

	/**Setter de l'�nigme r�solue et de l'image apr�s r�solution de l'�nigme
	 * 
	 */
	public void enigmeResolue() {
		this.enigmeresol = true;
		super.setNomImage(imageCoffreOPorte);
		
	}
	
	
}
